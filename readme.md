ET Lead Management
Contributors: jcastaneda
Tags: form, customer-relation, management, leads
Requires at least: 4.4
Tested up to: 4.9-alpha
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl.html

ET Lead Managament is a simple pluginn.

== Description ==
Creates a shortcode [crm-form] in which you can use in posts, pages, or in a widget. A form is generated and those submissions get saved to the database as customers. You can edit and modify from the Dashboard.