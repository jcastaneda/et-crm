// dat file.
jQuery( document ).ready(function( $ ){
	// Let us get the button! my button!
	var submit  = $( 'form[id*="et-form-"] input[type="submit"]' );
	var form = $( "form[id*='et-form-']" );

	submit.on( 'click', function(e){
		// prevent default behaviour so we handle it all via AJAX.
		e.preventDefault();

		var name     = $( 'form[id*="et-form-"] input[name=name]' ).val();
		var number   = $( 'form[id*="et-form-"] input[name=number]' ).val();
		var email    = $( 'form[id*="et-form-"] input[name=email]' ).val();
		var budget   = $( 'form[id*="et-form-"] input[name=budget]' ).val();
		var message  = $( 'form[id*="et-form-"] textarea[name=message]' ).val();
		var stamp    = new Date( $.now() );

		// Check to see if name and message are filled out.
		if ( name !== '' && message !== '' ) {
			// hide the fieldset.
			form.find( "fieldset" ).hide();

			// load our animation so user knows something is happening.
			$( "#loader" ).show();

			// create a data object.
			var data = {
				'action'   : 'et_new_customer',
				'name'     : name,
				'number'   : number,
				'email'    : email,
				'budget'   : budget,
				'message'  : message,
				'datetime' : stamp,
				'et_nonce' : etObj.nonce
			};

			// make our POST request.
			$.post( etObj.ajaxurl, data, function(response) {
				// handle the response accordingly.
				if( response.success ) {
					// because we succeeded we show a success message.
					$( "#loader" ).hide();
					form.append( "<p>" + etObj.success + "</p>" );
				} else {
					// othewise we show some error.
					form.show();
					form.prepend( "<p>" + etObj.error + "</p>")
				}
			});
		} else {
			form.prepend( "<p class='warning'>" + etObj.warning + "</p>" )
			form.find(".warning").delay( 1500 ).fadeOut();
		}
	});
});