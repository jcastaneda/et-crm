<?php
/**
 * Plugin Name: ET CRM
 * Description: things and stuff.
 * Author: Jose castaneda
 * Version: 1.0.0
 * Text Domain: et-crm
 */

add_shortcode( 'crm-form', 'etcrm_get_form' );
/**
 * Returns the form's  markup.
 * @param  array $atts Associative array
 * @return string       The HTML markup of the final form
 *                          to be rendered on the front-end.
 */
function etcrm_get_form( $atts ) {
	// create attributes for the shortcode.
	$defaults = array(
		'name'    => __( 'Name', 'et-form' ),
		'number'  => __( 'Phone Number' , 'et-form' ),
		'email'   => __( 'Email Address', 'et-form' ),
		'budget'  => __( 'Desired Budget', 'et-form' ),
		'message' => __( 'Message', 'et-form' ),
		'cols'    => 50,
		'rows'    => 10,
		);

	// allow filtering of shortcode attributes.
	$atts = shortcode_atts( $defaults, $atts, 'crom-form' );

	// create unique ID for the form.
	$instance = 0;

	$inputs = '<fieldset class="fields">';
	$spinner = '<div id="loader" class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div><div class="cssload-shaft4"></div><div class="cssload-shaft5"></div><div class="cssload-shaft6"></div><div class="cssload-shaft7"></div><div class="cssload-shaft8"></div><div class="cssload-shaft9"></div><div class="cssload-shaft10"></div></div>';

	// The fields for the form.
	$fields = array(
		'name'    => sprintf( '<label for="name">%s<input type="text" name="name" aria-required="true"></label>', esc_html( $atts['name'] ) ),
		'number'  => sprintf( '<label for="number">%s<input type="tel" name="number"></label>', esc_html( $atts['number'] ) ),
		'email'   => sprintf( '<label for="email">%s<input type="email" name="email"></label>', esc_html( $atts['email'] ) ),
		'budget'  => sprintf( '<label for="budget">%s<input type="number" name="budget"></label>', esc_html( $atts['budget'] ) ),
		'message' => sprintf( '<label for="message">%s<textarea name="message" cols="%s" rows="%s" aria-required="true"></textarea></label>', esc_html( $atts['message'] ), absint( $atts['cols'] ), absint( $atts['rows'] ) ),
		'submit'  => sprintf( '<input type="submit" class="button submit" id="et-submit-%s">', $instance ),
		);

	/**
	 * Filter the input fields of the form.
	 * @var array
	 */
	$fields = apply_filters( 'et_form_fields', $fields );

	$inputs .= join( '', $fields );
	$inputs .= '</fieldset>';

	// Create filterable classes for the overall form.
	$classes = (string) apply_filters( 'et_form_classes', 'et-form' );

	// Action for extenti
	do_action( 'et_pre_form', get_the_ID() );
	// create the overall form.
	$form = sprintf( '<form id="et-form-%s" class="%s" method="POST">%s</form>%s', $instance, $classes, $inputs, $spinner );

	// Action for extenti
	do_action( 'et_after_form', get_the_ID() );

	// increase the instance.
	$instance++;

	// finally return the form.
	return $form;
}

// AJAX
// no ajax handler are you!
add_action( 'wp_ajax_nopriv_et_new_customer', 'et_form_handler' );
add_action( 'wp_ajax_et_new_customer', 'et_form_handler' );
/**
 * Handles the formm submissiona. Inserts a new post to the 'customer'
 * post type and additional meta.
 * @return mixed Sends JSON error or success message.
 */
function et_form_handler() {

	check_ajax_referer( 'et-nonce-form', 'et_nonce' );
	// validate things
	$postarr = array(
		'post_content' => wp_kses_post( $_POST['message'] ),
		'post_title'   => sanitize_title_with_dashes( $_POST['name'] ),
		'post_status'  => 'publish',
		'post_type'    => 'customer',
	);

	// handle the insertion of the post
	$id = wp_insert_post( $postarr, false );

	// check to see if insertion was success
	if ( ! is_wp_error( $id ) ) {
		// meta_number - post_meta
		add_post_meta( $id, 'customer_number', strip_tags( $_POST['number'] ) );

		// meta_email - post_meta
		add_post_meta( $id, 'customer_email', sanitize_email( $_POST['email'] ) );

		// meta_budget - post_meta
		add_post_meta( $id, 'customer_budget', absint( $_POST['budget'] ) );

		// meta_datetime - post_meta
		add_post_meta( $id, 'customer_datetime', strip_tags( $_POST['datetime'] ) );

		// send success message.
		wp_send_json_success();
	} else {
		wp_send_json_error();
	}

	// we all fall down!!
	die;
}


// JS et CSS
add_action( 'wp_enqueue_scripts', 'et_front_end' );
/**
 * Enqueues JS and CSS, adds translation ready for JS.
 */
function et_front_end() {
	wp_enqueue_script( 'et-js-handler', plugins_url( '/js/handler.js', __FILE__ ), array( 'jquery' ) );

	// JS object.
	$obj = array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'nonce'   => wp_create_nonce( 'et-nonce-form' ),
		'warning' => __( 'Please fill out name and message', 'et-crm' ),
		'success' => __( 'Your submission was successful', 'et-crm' ),
		'error'   => __( 'There was an error submitting the form', 'et-crm' ),
	);
	wp_localize_script( 'et-js-handler', 'etObj', $obj );

	// styles.
	wp_enqueue_style( 'et-css-spinner', plugins_url( '/css/spinner.css', __FILE__ ) );
	wp_enqueue_style( 'et-form-css', plugins_url( '/css/style.css', __FILE__ ) );
}

// register the post type. Making sure that it is not
// publicly queryable. Will need to look at using custom metabox
// to display the input from the form.
function et_register_post_type() {
	// Array of labels used for the post type.
	$labels = array(
		'name'                  => __( 'Customers', 'et-crm' ),
		'singular_name'         => __( 'Customer', 'et-crm' ),
		'menu_name'             => __( 'Customers', 'et-crm' ),
		'name_admin_bar'        => __( 'Customers', 'et-crm' ),
		'add_new'               => __( 'Add New Customer', 'et-crm' ),
		'add_new_item'          => __( 'Add New Customer', 'et-crm' ),
		'edit_item'             => __( 'Edit Customer', 'et-crm' ),
		'new_item'              => __( 'New Customer', 'et-crm' ),
		'view_item'             => __( 'View Customer', 'et-crm' ),
		'search_items'          => __( 'Search Customers', 'et-crm' ),
		'not_found'             => __( 'No customers found', 'et-crm' ),
		'not_found_in_trash'    => __( 'No customers found in trash', 'et-crm' ),
		'all_items'             => __( 'All customers', 'et-crm' ),
		// optional setting that could be used down the road.
		'featured_image'        => __( 'Profile Image', 'et-crm' ),
		'set_featured_image'    => __( 'Set profile image', 'et-crm' ),
		'remove_featured_image' => __( 'Remove profile image', 'et-crm' ),
		'use_featured_image'    => __( 'Use as profile image', 'et-crm' ),
		'insert_into_item'      => __( 'Insert into post', 'et-crm' ),
		'uploaded_to_this_item' => __( 'Uploaded to this post', 'et-crm' ),
		'views'                 => __( 'Filter customers list', 'et-crm' ),
		'pagination'            => __( 'Customers list navigation', 'et-crm' ),
		'list'                  => __( 'Customers list', 'et-crm' ),
	);

	// main array of args to be passed for registering the post type.
	$post = array(
		'labels'               => $labels,
		'public'               => true,
		'publicly_queryable'   => false,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'query_var'            => true,
		'capability_type'      => 'post',
		'capabilities'         => array(
			'create_posts' => false,
			),
		'map_meta_cap'         => true,
		'has_archive'          => false,
		'hierarchical'         => false,
		'menu_position'        => 30,
		'supports'             => array( 'title', 'editor' ),
		'delete_with_user'     => false,
		'register_meta_box_cb' => 'et_customer_metaboxes',
	);

	// Actually register the post type
	register_post_type( 'customer', $post );

	// register a tag for the post type.
	register_taxonomy( 'customer_tag', 'customer', array(
		'label'              => __( 'Customer tags', 'et-crm' ),
		'publicly_queryable' => false,
		'hierarchical'       => false,
		'show_in_nav_menus'  => false,
		'show_tagcloud'      => false,
	) );

	// register a category taxonomy for our post type.
	register_taxonomy( 'customer_cat', 'customer', array(
		'labels'             => array(
			'name'          => __( 'Customer categories', 'et-crm' ),
			'singular_name' => __( 'Customer Category', 'et-crm' ),
			'search_items'  => __( 'Search Customer Categories', 'et-crm' ),
			'all_items'     => __( 'All Customer Categories', 'et-crm' ),
			'edit_item'     => __( 'Edit Customer Category', 'et-crm' ),
			'view_item'     => __( 'View Customer Category', 'et-crm' ),
			'update_item'   => __( 'Update Customer Category', 'et-crm' ),
			'add_new_item'  => __( 'Add Customer Category', 'et-crm' ),
			'not_found'     => __( 'No Customer Categories Found', 'et-crm' ),
			),
		'publicly_queryable' => false,
		'hierarchical'       => false,
		'show_in_nav_menus'  => false,
		'show_tagcloud'      => false,
	) );
}
add_action( 'init', 'et_register_post_type' );

/**
 * The callback function from post type registration.
 */
function et_customer_metaboxes() {
	// post meta for the customer.
	add_meta_box( 'et-meta', __( 'Customer Details', 'et-form' ), 'et_render_metabox' );

	// remove the slug metabox
	remove_meta_box( 'slugdiv', 'customer', 'normal' );
}

/**
 * Renders the metabox on the customer edit page.
 */
function et_render_metabox( $post ) {
	$email = get_post_meta( $post->ID, 'customer_email', true );
	$number = get_post_meta( $post->ID, 'customer_number', true );
	$budget = get_post_meta( $post->ID, 'customer_budget', true );
	wp_nonce_field( basename( __FILE__ ), 'et_meta_nonce' );
	?>
	<p><?php
	/* Translators: a time stamp */
	printf( esc_html__( 'Date Submitted: %s', 'et-crm' ), get_post_meta( $post->ID, 'customer_datetime', true ) );
	?></p>
	<p>
		<span><?php esc_html_e( 'Email:', 'et-crm' ); ?></span>
		<input type="text" name="email" value="<?php echo esc_attr( $email );?>">
	</p>
	<p>
		<span><?php esc_html_e( 'Phone number:', 'et-crm' ); ?></span>
		<input type="text" name="number" value="<?php echo esc_attr( $number ); ?>">
	</p>
	<p>
		<span><?php esc_html_e( 'Budget:', 'et-crm' ); ?></span>
		<input type="text" name="budget" value="<?php echo esc_attr( $budget ); ?>">
	</p>
<?php }

add_action( 'save_post_customer', 'et_edit_customer', 10, 3 );
function et_edit_customer( $post_id, $post, $update ) {

	if ( ! isset( $_POST['et_meta_nonce'] ) || ! wp_verify_nonce( $_POST['et_meta_nonce'], basename( __FILE__ ) ) ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
	}

	update_post_meta( $post_id, 'customer_email', sanitize_email( $_POST['email'] ) );
	update_post_meta( $post_id, 'customer_number', sanitize_text_field( $_POST['number'] ) );
	update_post_meta( $post_id, 'customer_budget', absint( $_POST['budget'] ) );

	return $post_id;
}
